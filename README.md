# OBR

An Opensource Backup Restorer utility

## Problem Statement
You have to design a backup restore utility, make sure that you follow all the best practices that should be followed while creating a successful utility such as:
  - Design: You should design your complete solution first, Design document if possible
  - Flexibility/Extensibility: Solution should be flexible i.e it should be easy to add new features
  - Readability: Code should be easily readable so it will be easy to maintain & manage
  - Ease of use: Usage of utility should be fairly easy.

## Feature Sets
Below is the set of features that your utility should support:
  - It should be able to support MySQL DB & Filesystem
  - It should have capability to enable Encryption & Compression
  - It should be able to support remote server & S3 bucket as backup storage
  - It should support configuring the lifecycle of backups
  - It should have reporting built into it

## Reference
![](OBR.png) 

## Slack Invitation

[![](Slack.png)](https://join.slack.com/t/opstree/shared_invite/enQtMjg3ODUyMDgwOTY1LTUwNjMxNGQ4MmNiNDU5MjA2NGRjYjZmM2E5YWRiMDU3MDcwMzc1MzFhM2NhZGZhZTYwOTQ5MWE4MzJlODEzNWM)


## How we will work
* You can form a team of maximum 3 people
* Fork this repo and then submit a PR with a branch name corresponding to your team
* Final selected solution will be merged into master branch
* Post event we will form a team who will continue with this utility

## Teams
* RONINS: Arpit, Bimlesh, Shatrujeet
* SAMURAI: Kirti, Adeel, Ishan
* GROOT: Ankur, Ankit, Rajat Ravi
* SHAAKAL: Saurabh, Sumit, Anita
* IRONMAN: Harsh, Ekansh, Hemant
* NYD: Ashish, Rachit, Ankit
* THOR: Abhishek, Amit